<?php 

class Mforum extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function get_side_forum(){
		$data = array();
		$this->db->where('active', 'Y');
		$query = $this->db->get('cat_forum');
		if ($query->num_rows>0) {
			foreach ($query->result_array() as $row ) {
				$data[$row['id_cat_forum']] = $row['name_forum'];
			}
		}

		return $data;


	}

	function get_all_forum()
	{
		$this->db->select('*');
		$this->db->where('active', 'Y');
		$this->db->from('cat_forum');
		$query = $this->db->get();
		return $query;
	}

	function cat_forum_sub($id_cat_forum)
	{
		$query_berita=$this->db->query("SELECT * FROM cat_forum_sub WHERE cat_forum_sub.kat_status_sub='1' AND cat_forum_sub.id_cat_forum = '$id_cat_forum' ORDER BY cat_forum_sub ASC");
		return $query_berita->result_array();
	}
	function Topik_forum($id_cat_forum_sub)
	{
		$this->db->select('id_cat_forum_sub');
		$this->db->where('id_cat_forum_sub', $id_cat_forum_sub);
		$this->db->from('forum_question');
		$totalRecord = $this->db->count_all_results();
		return $totalRecord;
	}

	function get_by_category($start, $limit, $cat_id)
	{
		$cat_string = "(";
			foreach ($cat_id as $key => $id) {
				if ($key == 0) {
					$cat_string .= " a.category_id = ".$id;
				} else {
					$cat_string .= " OR a.category_id = ".$id;
				}
			}
			$cat_string .= ")";
	        $sql = "SELECT a.*, b.name as category_name, b.slug as category_slug, c.date_add 
                FROM ".TBL_THREAD." a, ".TBL_CAT." b, ".TBL_POST." c 
                WHERE a.category_id = b.id AND a.id = c.thread_id AND ".$cat_string." 
                AND c.date_add = (SELECT MAX(date_add) FROM ".TBL_POST." WHERE thread_id = a.id LIMIT 1) 
                ORDER BY c.date_add DESC LIMIT ".$start.", ".$limit;
        return $this->db->query($sql)->result();
	}

	function get_total_by_category($cat_id)
	{
		$cat_string = "(";
			foreach ($cat_id as $key => $id) {
				if ($key == 0) {
					$cat_string .= " a.category_id = ".$id;
				} else {
					$cat_string .= " OR a.category_id = ".$id;
				}
			}

			$cat_string .= ")";

		$sql = "SELECT a.* FROM ".TBL_THREAD." a WHERE ".$cat_string;
        return $this->db->query($sql)->num_rows();
	}

}

?>
	