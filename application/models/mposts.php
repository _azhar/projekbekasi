<?php 

class Mposts extends CI_Model
{
		

	function __construct()
	{
		parent::__construct();
	
	}

	function get_live_post($limit)
	{
		$data = array();
		$this->db->limit($limit);
		$this->db->where('status','published');
		$this->db->order_by('pubdate','desc');
		$query = $this->db->get('post');
	}

	function posting()
	{
		$this->db->where('status','published');
		$query = $this->db->get('post')->num_rows();
		return $query;
	}

	function teraktual(){
		$this->db->select('id,title,link');
		$this->db->from('post');
		$this->db->where('teraktual','Y');
		$query = $this->db->get();
		return $query;
	}

	function inspiratif(){
		$this->db->select('id,title,link');
		$this->db->from('post');
		$this->db->where('inspiratif','Y');
		$query = $this->db->get();
		return $query;
	}

	function bermanfaat(){
		$this->db->select('id,title');
		$this->db->from('post');
		$this->db->where('bermanfaat','Y');
		$query = $this->db->get();
		return $query;
	}

	function menarik(){
		$this->db->select('id,title');
		$this->db->from('post');
		$this->db->where('menarik','Y');
		$query = $this->db->get();
		return $query;
	}

	function menunav(){
		$this->db->select('nama_menu,link');
		$this->db->from('menunav');
		$this->db->where('aktif','Y');
		$query = $this->db->get();
		return $query;
	}
	function get_posts($id)
	{
		$data = array();

		$options = array('id' => $id);
		$query = $this->db->get_where('post',$options,1);
		if ($query->num_rows() > 0) {
			$data = $query->row_array();
		}
		$query->free_result();
		return $data;

	}

	function get_all_categories()
	{	
		$data = array();
		$Q = $this->db->get('category_news');
		if ($Q->num_rows() > 0) {
			foreach ($Q->result_array() as $row) {
				$data[$row['id']] = $row['name'];
			}
		}
		$Q->free_result();
		return $data;
	}

	function get_categories($id)
	{
	
	$data = array();
    $options = array('id' => $id);
    $Q = $this->db->get_where('category_news',$options,1);
    if ($Q->num_rows() > 0){
      $data = $Q->row_array();
    }
    return $data;  

	}


	function get_all_post_categories($pp,$uri, $id)
	{
		$this->db->select('*');
		$this->db->where('category_id',$id);
		$this->db->where('status', 'published');
		$this->db->order_by('pubdate', 'desc');
		$query = $this->db->get('post',$pp,$uri);
		return $query;
	}

	function get_category_total($catid)
	{
		$this->db->where('category_id', $catid);
		$this->db->where('status', 'Y');
		$query = $this->db->get('post')->num_rows();
		return $query;
	}

	function add_post($image, $id)
	{
		$data = array(
			'title'=> $this->input->post('title')
			);
	}

	function get_all_post()
	{
		$this->db->order_by('pubdate', 'desc');
		$this->db->where('status', 'published');
		$query = $this->db->get('post');
		return $query;
	}

	function get_total_cat_thread()
	{
		$query = $this->db->get('category');
		return $query;
	}
	public function category_get_all($cat_id = 0)
    {   
        $this->data = array();
        // $this->db->where('user_id',$id);
        $this->db->order_by('name', 'asc');
        $query = $this->db->get_where(TBL_CAT, array('parent_id' => $cat_id));
        $counter = 0;
        foreach ($query->result() as $row) {
                $this->data[$counter]['id'] = $row->id;
            $this->data[$counter]['parent_id'] = $row->parent_id;
            $this->data[$counter]['name'] = $row->name;
            $this->data[$counter]['slug'] = $row->slug;
            $this->data[$counter]['real_name'] = $row->name;

            $children = $this->category_get_children($row->id, ' - ', $counter);
            $counter = $counter + $children;
            $counter++;
        }        
        return $this->data;
    }

    function post_get_all($id)
    {
    	$query = $this->db->get_where(TBL_POST, array('author_id' => $id));
    	return $query->result_array();
    }


    public function category_get_children($id, $separator, $counter)
	{
        $this->db->order_by('name', 'asc');
		$query = $this->db->get_where(TBL_CAT, array('parent_id' => $id));
		if ($query->num_rows() == 0)
		{
			return FALSE;
		}
		else
		{
			foreach($query->result() as $row)
			{
				$counter++;
				$this->data[$counter]['id'] = $row->id;
				$this->data[$counter]['parent_id'] = $row->parent_id;
                $this->data[$counter]['name'] = $separator.$row->name;
                $this->data[$counter]['slug'] = $row->slug;
                $this->data[$counter]['real_name'] = $row->name;
				$children = $this->category_get_children($row->id, $separator.' - ', $counter);

				if ($children != FALSE)
				{
					$counter = $counter + $children;
				}
			}
			return $counter;
		}
	}
}

?>