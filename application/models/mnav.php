<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mnav extends CI_Model {

	

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all_sidebars()
	{	
		$this->db->where('aktif', 'Y');
		$query = $this->db->get(TBL_MENU)->result_array();
		return $query;
	}

		public function get_all_sidebars2()
	{	
		$this->db->where('aktif', 'Y');
		$query = $this->db->get(TBL_MENU2)->result_array();
		return $query;
	}

		public function get_all_sidebars3()
	{	
		$this->db->where('aktif', 'Y');
		$query = $this->db->get(TBL_MENU3)->result_array();
		return $query;
	}
}

/* End of file mnav.php */
/* Location: ./application/models/mnav.php */