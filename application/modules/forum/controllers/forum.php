<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum extends CI_Controller {
    public $page_config  = array();
    public $data         = array();
	function __construct()

	{
		parent::__construct();	
		$this->load->model('mposts');
		$this->load->model('mforum');
		$this->load->model('mthread');
		$this->load->model('madmin');
		$this->load->library('tank_auth_groups','','tank_auth');
		$this->lang->load('tank_auth');
	}

	public function index($start = 0)
	{
		if ($this->tank_auth->is_logged_in()) {
        	$inisial['nama']	= $this->tank_auth->get_username();
        }else {
        	$inisial['nama']	= "GUEST";
        }
		

			$this->page_config['base_url']    = site_url('welcome/index/');
       		$this->page_config['uri_segment'] = 3;
        	$this->page_config['total_rows']  = $this->db->count_all(TBL_THREAD);;
        	$this->page_config['per_page']    = 10;
        
        	$this->set_pagination();
        
        	$this->pagination->initialize($this->page_config);
        	$thread['page']    = $this->pagination->create_links();
        	$menu['menunav']=$this->mposts->menunav();
        	$side['categories']    = $this->madmin->category_get_all();
        	$thread['threads'] = $this->mthread->get_all($start, $this->page_config['per_page']);
        	$data['menunav_view']=$this->load->view('main/menunav',$menu,TRUE);
			$data['header'] =$this->load->view('main/header',$inisial, TRUE);
			$data['footer'] =$this->load->view('main/footer','', TRUE);
			$data['view'] =$this->load->view('forum/view_forum',$thread, TRUE);
			$data['side'] =$this->load->view('side_menu',$side, TRUE);
			$this->load->view('view',$data);

	}
	public function set_pagination()
    {
        $this->page_config['first_link']         = '&lsaquo; First';
        $this->page_config['first_tag_open']     = '<li>';
        $this->page_config['first_tag_close']    = '</li>';
        $this->page_config['last_link']          = 'Last &raquo;';
        $this->page_config['last_tag_open']      = '<li>';
        $this->page_config['last_tag_close']     = '</li>';
        $this->page_config['next_link']          = 'Next &rsaquo;';
        $this->page_config['next_tag_open']      = '<li>';
        $this->page_config['next_tag_close']     = '</li>';
        $this->page_config['prev_link']          = '&lsaquo; Prev';
        $this->page_config['prev_tag_open']      = '<li>';
        $this->page_config['prev_tag_close']     = '</li>';
        $this->page_config['cur_tag_open']       = '<li class="active"><a href="javascript://">';
        $this->page_config['cur_tag_close']      = '</a></li>';
        $this->page_config['num_tag_open']       = '<li>';
        $this->page_config['num_tag_close']      = '</li>';
    }
	function category($slug, $start=0)
	{	
		$category = $this->db->get_where(TBL_CAT, array('slug'=>$slug))->row();
		$this->load->model('madmin');
		$this->data['cat'] = $this->madmin->category_get_all_parent($category->id,0);
		$this->data['thread'] = $category;

		$cat_id = array();
		$child_cat = $this->madmin->category_get_all($category->id);
		$cat_id[0] = $category->id;
		foreach ($child_cat as $cat) {
				$cat_id[] = $cat['id'];
			}	

		$this->page_config['base_url'] = site_url('fazharamirorum/category/'.$slug);
		$this->page_config['uri_segment'] = 4;
		$this->page_config['total_rows'] = $this->mforum->get_total_by_category($cat_id);
		$this->page_config['per_page']    = 10;
		$this->set_pagination();
		$this->pagination->initialize($this->page_config);
      	$thread['page']    = $this->pagination->create_links();
        $menu['menunav']=$this->mposts->menunav();

        if ($this->session->userdata('user_id')) {
        	$inisial['nama']	= $this->tank_auth->get_username();
        }else {
        	$inisial['nama']	= "GUEST";
        }
		
        $thread['threads'] = $this->mforum->get_by_category($start, $this->page_config['per_page'],$cat_id);
        $data['menunav_view']=$this->load->view('main/menunav',$menu,TRUE);
		$data['header'] =$this->load->view('main/header',$inisial, TRUE);
		$data['footer'] =$this->load->view('main/footer','', TRUE);
		$data['view'] =$this->load->view('view_forum',$thread, TRUE);
		$side['categories']    = $this->madmin->category_get_all();
		$data['side'] =$this->load->view('side_menu',$side, TRUE);
		$this->load->view('view',$data);
	}

	public function talk($slug, $start = 0)
	{
		if ($this->input->post('btn-post')) {
			if (!$this->session->userdata('user_id')) {
				redirect('auth/login');
			} else if ($this->session->userdata('user_id') == 0) {
				redirect('forum');
			}

			$this->mthread->reply();

		if ($this->mthread->error_count != 0) {
		
			$this->data['error'] = $this->mthread->error;
		
		} else {	
		
			$this->session->set_userdata('tmp_success',1);
		
			redirect('forum/talk/'.$slug.'/'.$start);
		}

		}

		$tmp_success_new = $this->session->userdata('tmp_success');

		if ($tmp_success_new != null) {
			// new thread created			
			$this->session->set_userdata('tmp_success_new');
			
			$this->data['tmp_success_new'] = 1;
		}

		$tmp_success_new = $this->session->set_userdata('tmp_success');
		if ($tmp_success_new != null) {
			$this->session->unset_userdata('tmp_success');
			$this->data['tmp_success']=1;
		}

		 $thread = $this->db->get_where(TBL_THREAD, array('slug' => $slug))->row();
        
        // set pagination
        $this->load->library('pagination');
        $this->page_config['base_url']    = site_url('forum/talk/'.$slug);
        $this->page_config['uri_segment'] = 4;
        $this->page_config['total_rows']  = $this->db->get_where(TBL_POST, array('thread_id' => $thread->id))->num_rows();
        $this->page_config['per_page']    = 10;
        
        $this->set_pagination();
        
        $this->pagination->initialize($this->page_config);

        if ($this->session->userdata('user_id')) {
        	$inisial['nama']	= $this->tank_auth->get_username();
        }else {
        	$inisial['nama']	= "GUEST";
        }

        $posts  = $this->mthread->get_posts($thread->id, $start, $this->page_config['per_page']);
        //$this->thread_model->get_posts_threaded($thread->id, $start, $this->page_config['per_page']);
        $this->load->model('madmin');
        $this->data['cat']    = $this->madmin->category_get_all_parent($thread->category_id, 0);
        $menu['menunav']=$this->mposts->menunav();
		$this->data['menunav_view']=$this->load->view('main/menunav',$menu,TRUE);
        $this->data['categories']    = $this->madmin->category_get_all();
        $this->data['page']   = $this->pagination->create_links();
        $this->data['thread'] = $thread;
        $this->data['posts']  = $posts;
        $side['categories']    = $this->madmin->category_get_all();
		$this->data['side'] =$this->load->view('side_menu',$side, TRUE);
        $this->load->view('main/header',$inisial);
        $this->load->view('talk', $this->data);
        $this->load->view('footer');
	}

	function login()
	{
		if ($this->tank_auth->is_logged_in()) {									// logged in
			redirect('/welcome/welcome');

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/auth/send_again/');

		} else {
			$data['login_by_username'] = ($this->this->page_config->item('login_by_username', 'tank_auth') AND
					$this->this->page_config->item('use_username', 'tank_auth'));
			$data['login_by_email'] = $this->this->page_config->item('login_by_email', 'tank_auth');

			$this->form_validation->set_rules('login', 'Login', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('remember', 'Remember me', 'integer');

			// Get login for counting attempts to login
			if ($this->this->page_config->item('login_count_attempts', 'tank_auth') AND
					($login = $this->input->post('login'))) {
				$login = $this->security->xss_clean($login);
			} else {
				$login = '';
			}

			$data['use_recaptcha'] = $this->this->page_config->item('use_recaptcha', 'tank_auth');
			if ($this->tank_auth->is_max_login_attempts_exceeded($login)) {
				if ($data['use_recaptcha'])
					$this->form_validation->set_rules('recaptcha_response_field', 'Confirmation Code', 'trim|xss_clean|required|callback__check_recaptcha');
				else
					$this->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
			}
			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if ($this->tank_auth->login(
						$this->form_validation->set_value('login'),
						$this->form_validation->set_value('password'),
						$this->form_validation->set_value('remember'),
						$data['login_by_username'],
						$data['login_by_email'])) {								// success
					redirect('');

				} else {
					$errors = $this->tank_auth->get_error_message();
					if (isset($errors['banned'])) {								// banned user
						$this->_show_message($this->lang->line('auth_message_banned').' '.$errors['banned']);

					} elseif (isset($errors['not_activated'])) {				// not activated user
						redirect('/auth/send_again/');

					} else {													// fail
						foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);
					}
				}
			}
			$data['show_captcha'] = FALSE;
			if ($this->tank_auth->is_max_login_attempts_exceeded($login)) {
				$data['show_captcha'] = TRUE;
				if ($data['use_recaptcha']) {
					$data['recaptcha_html'] = $this->_create_recaptcha();
				} else {
					$data['captcha_html'] = $this->_create_captcha();
				}
			}
			$this->load->view('auth/login_form', $data);
		}
	}

}
/* End of file index.php */
/* Location: ./application/controllers/index.php */
?>