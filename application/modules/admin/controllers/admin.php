<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	 public $data         = array();
    public $page_config  = array();

	function __construct()
	{
		parent::__construct();
		$this->load->model('mnav');
		$this->load->model('mposts');
		$this->load->model('mthread');
		$this->load->model('madmin');
        $this->madmin->check_role();
	}

	 function index()
	{
			

		$tmp_success_del = $this->session->userdata('tmp_success_del');
        if ($tmp_success_del != NULL) {
            // role deleted
            $this->session->unset_userdata('tmp_success_del');
            $this->data['tmp_success_del'] = 1;
        }
        
        $side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
         $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3();
        $cat['categories'] = $this->madmin->category_get_all();
        $cat['get_all_posts'] = $this->mthread->get_thread_all();
        $this->load->view('header', $this->data);
        $this->load->view('topbar');
        $this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('news/home_news',$cat);
        $this->load->view('category/home_category',$cat);
        $this->load->view('footer');
	}

	function news()
	{	
		$cat['cats'] = $this->mposts->category_get_all();
        $cat['start'] = 0;
		$cat['get_all_posts'] = $this->mthread->get_thread_all();
         $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3();
		$this->load->view('header');
    	$this->load->view('topbar');
    	$side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
    	$this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('news/home_news', $cat);;
    	$this->load->view('footer');
	}

    function post(){
        $session_data = $this->session->userdata('user_id');
        $id = $session_data['id'];
         $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3();

        $cat['get_post'] = $this->mposts->post_get_all($id);
        $this->load->view('header');
        $this->load->view('topbar');
        $side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
        $this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('post/home_post', $cat);;
        $this->load->view('footer');
    }

    function news_edit($thread_id)
    {
        if ($this->input->post('btn-save'))
        {
            $this->madmin->thread_edit();
            if ($this->madmin->error_count != 0) {
                $news['error']    = $this->madmin->error;
            } else {
                $this->session->set_userdata('tmp_success', 1);
                redirect('admin/news');
            }
        }
         $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3();
        $news['cats'] = $this->mposts->category_get_all(); 
        $news['thread']  = $this->db->get_where(TBL_THREAD, array('id' => $thread_id))->row();
        $this->load->view('header');
        $this->load->view('topbar');
        $side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
        $this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('news/news_edit', $news);;
        $this->load->view('footer');

    }

    function news_delete($thread_id)
    {
        // delete thread
        $this->db->delete(TBL_THREAD, array('id'=> $thread_id));

        // delete all pos on the thread

        $this->db->delete(TBL_POST, array('thread_id'=>$thread_id));
        $this->session->set_userdata('tmp_success_del',1);
        redirect('admin/news');
    }

	  function set_pagination()
    {
    	$this->page_config['first_link']         = '&lsaquo; First';
        $this->page_config['first_tag_open']     = '<li>';
        $this->page_config['first_tag_close']    = '</li>';
        $this->page_config['last_link']          = 'Last &raquo;';
        $this->page_config['last_tag_open']      = '<li>';
        $this->page_config['last_tag_close']     = '</li>';
        $this->page_config['next_link']          = 'Next &rsaquo;';
        $this->page_config['next_tag_open']      = '<li>';
        $this->page_config['next_tag_close']     = '</li>';
        $this->page_config['prev_link']          = '&lsaquo; Prev';
        $this->page_config['prev_tag_open']      = '<li>';
        $this->page_config['prev_tag_close']     = '</li>';
        $this->page_config['cur_tag_open']       = '<li class="active"><a href="javascript://">';
        $this->page_config['cur_tag_close']      = '</a></li>';
        $this->page_config['num_tag_open']       = '<li>';
        $this->page_config['num_tag_close']      = '</li>';
    }

    public function add_thread()
    {
    	 if (!$this->session->userdata('user_id')) {
          redirect('auth/login');
      }
       // } else if ($this->session->userdata('thread_create') == 0) {
       //     redirect('thread');
       // }

        if ($this->input->post('btn-create')) {
            $this->mthread->create();
            if ($this->mthread->error_count != 0) {
                $cat['error']    = $this->mthread->error;
            } else {
                $this->session->set_userdata('tmp_success_new', 1);
                redirect('forum/talk/'.$this->mthread->fields['slug']);
            }
        }
        $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3();
    	$cat['cats'] = $this->mposts->category_get_all();
        $this->load->view('header');
    	$this->load->view('topbar');
    	$side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
    	$this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('news/news_create', $cat);
    	$this->load->view('footer');
    }
    function _set_rule()
	{
		$this->form_validation->set_rules('title','Title','required|xss_clean');
		$this->form_validation->set_rules('category_id','Category_id','required|xss_clean');
		$this->form_validation->set_rules('slug','Slug','required|xss_clean');
		$this->form_validation->set_rules('date_add','Date_edit','required|xss_clean');
		$this->form_validation->set_rules('date_last_post','Date_last_post','required|xss_clean');
	}

	function category()
	{
		$tmp_success_del = $this->session->userdata('tmp_success_del');
        if ($tmp_success_del != NULL) {
            // role deleted
            $this->session->unset_userdata('tmp_success_del');
            $this->data['tmp_success_del'] = 1;
        }
        $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3();
        $side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
        $cat['categories'] = $this->madmin->category_get_all();
        $this->load->view('header', $this->data);
        $this->load->view('topbar');
        $this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('category/home_category',$cat);
        $this->load->view('footer');
	}

    function add_category()
    {
        if (!$this->session->userdata('user_id')) {
          redirect('auth/login');
      }
       // } else if ($this->session->userdata('thread_create') == 0) {
       //     redirect('thread');
       // }

        if ($this->input->post('btn-create')) {
            $this->madmin->category_create();
            if ($this->madmin->error_count != 0) {
                $cat['error']    = $this->madmin->error;
            } else {
                $this->session->set_userdata('tmp_success', 1);
                redirect('admin/category');
            }
        }
        $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3();
        $cat['cats'] = $this->madmin->category_get_all();
        $this->load->view('header');
        $this->load->view('topbar');
        $side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
        $this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('category/add_category', $cat);
        $this->load->view('footer');
    }

    function edit_category($cat_id)
    {

        if ($this->input->post('btn-edit')) {
            $this->madmin->category_edit();
            if ($this->madmin->error_count != 0) {
                $cat['error']    = $this->madmin->error;
            } else {
                $this->session->set_userdata('tmp_success', 1);
                redirect('admin/edit_category/'.$cat_id);
            }
        }

        $tmp_success = $this->session->userdata('tmp_success');
        if ($tmp_success != NULL) {
            // new category created
            $this->session->unset_userdata('tmp_success');
            $cat['tmp_success'] = 1;
        }
        $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3();
        $cat['cats'] = $this->madmin->category_get_all();
        $cat['category'] = $this->db->get_where(TBL_CAT, array('id' => $cat_id))->row();
        $this->load->view('header');
        $this->load->view('topbar');
        $side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
        $this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('category/edit_category', $cat);
        $this->load->view('footer');
    }

    function delete_category($cat_id)
    {
        $this->db->delete(TBL_CAT, array('id'=> $cat_id));
        $this->session->set_userdata('tmp_success_del',1);
        redirect('admin/category');
    }

    function user()
    {

        $this->db->order_by('username', 'asc');
        $cat['users'] = $this->db->get(TBL_USERS)->result_array();
        $side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
        $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3(); 
        $this->load->view('header');
        $this->load->view('topbar');
        $this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('user/home_user',$cat);
        $this->load->view('footer');
    }

    function view_role()
    {
        $tmp_success_del = $this->session->userdata('tmp_success_del');
       
         if ($tmp_success_del != NULL) {
             // role deleted
        $this->session->unset_userdata('tmp_success_del');
        $cat['tmp_success_del'] = 1;
         }

        $cat['roles'] = $this->madmin->role_get_all();
        $cat['column_width'] = floor(100/count($cat['roles']));
        $side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
        $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3(); 
        $this->load->view('header');
        $this->load->view('topbar');
        $this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('roles/home_role',$cat);
        $this->load->view('footer');
    }

    function role_edit($role_id)
    {
        // if ($this->session->userdata('group_id') == 300) {
        //     redirect('admin');
        // }
        if ($this->input->post('btn-edit')) {
            $this->madmin->role_edit();
            if ($this->madmin->error_count != 0) {                
                $cat['error'] = $this->madmin->error;
            } else {
                $this->session->set_userdata('tmp_success', 1);
                redirect('admin/role_edit/'.$role_id);
            }
        }
        $tmp_success = $this->session->userdata('tmp_success');
        if ($tmp_success != NULL) {
            // role updated
            $this->session->unset_userdata('tmp_success');
            $cat['tmp_success'] = 1;
        }

        $cat['role'] = $this->db->get_where(TBL_ROLES, array('id' => $role_id))->row();
        $side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
        $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3(); 
        $this->load->view('header');
        $this->load->view('topbar');
        $this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('roles/role_edit',$cat);
        $this->load->view('footer');
    }

    function role_create()
    {

     if ($this->session->userdata('role_create') == 0) {
            redirect('admin');
        }
        
        if ($this->input->post('btn-create'))
        {
            $this->madmin->role_create();
            if ($this->madmin->error_count != 0) {
                $cat['error']    = $this->madmin->error;
            } else {
                $this->session->set_userdata('tmp_success', 1);
                redirect('admin/role_create');
            }
        }
                                                        
        $tmp_success = $this->session->userdata('tmp_success');
        if ($tmp_success != NULL) {
            // new role created
            $this->session->unset_userdata('tmp_success');
            $cat['tmp_success'] = 1;
        }
        $side['get_all_sidebar'] = $this->mnav->get_all_sidebars(); 
        $side['get_all_sidebar2'] = $this->mnav->get_all_sidebars2(); 
        $side['get_all_sidebar3'] = $this->mnav->get_all_sidebars3(); 
        $this->load->view('header');
        $this->load->view('topbar');
        $this->load->view('sidebar',$side);
        $this->load->view('breadcumb');
        $this->load->view('roles/role_create',$cat);
        $this->load->view('footer');
    }
    function role_delete($role_id)
    {
        // if ($this->session->userdata('role_delete')==0) {
        //     redirect('admin');
        // }
        $this->db->delete(TBL_ROLES, array('id'=> $role_id));
        $this->session->set_userdata('tmp_success_del', 1);
        redirect('admin/view_role');
    }
}

/* End of file index.php */
/* Location: ./application/controllers/index.php */