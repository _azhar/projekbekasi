<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>News</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<meta name="author" content="Muhammad Usman">

	<!-- The styles -->
	<link id="bs-css" href="<?php echo base_url();?>assets/admin/css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/bootstrap-spacelab.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/bootstrap-cerulean.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/bootstrap-responsive.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/charisma-app.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/fullcalendar.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/fullcalendar.print.css" media='print'>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/chosen.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/uniform.default.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/colorbox.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery.cleditor.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery.noty.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/noty_theme_default.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/elfinder.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/elfinder.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery.iphone.toggle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/opa-icons.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/uploadify.css">

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="img/favicon.ico">
		
</head>