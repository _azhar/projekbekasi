

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<footer>
			<p class="pull-left">&copy; <a href="http://usman.it" target="_blank">Muhammad Usman</a> 2012</p>
			<p class="pull-right">Powered by: <a href="http://usman.it/free-responsive-admin-template">Charisma</a></p>
		</footer>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- transition / effect library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-tour.js"></script>
	<!-- library for cookie management -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/fullcalendar.min.js"></script>
	<!-- data table plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.dataTables.min.js"></script>

	<!-- chart libraries start -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/excanvas.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.pie.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.stack.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.resize.min.js"></script>

	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.chosen.min.js"></script>

	<!-- checkbox, radio, and file input styler -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.uniform.min.js"></script>

	<!-- plugin for gallery image view -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.colorbox.min.js"></script>

	<!-- rich text editor library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.cleditor.min.js"></script>

	<!-- notification plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.noty.js"></script>

	<!-- file manager library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.elfinder.min.js"></script>

	<!-- star rating plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.raty.min.js"></script>

	<!-- for iOS style toggle switch -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.iphone.toggle.js"></script>

	<!-- autogrowing textarea plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.autogrow-textarea.js"></script>

	<!-- multiple file upload plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.uploadify-3.1.min.js"></script>

	<!-- history.js for cross-browser state change on ajax -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.history.js"></script>

	<!-- application script for Charisma demo -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/charisma.js"></script>
</body>
</html>
