			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
						<h2><i class="icon-list-alt"></i><a href="<?php $this->config->item('base_url')?>admin/add_category">Create New Category</a></h2>
					</div>
			
			<?php if (count($categories)) {
				echo "
				<div class='box-content'>
					<table class='table table-striped table-bordered bootstrap-datatable '>
						<thead>
					 	  <tr>
					 	  	<th>No</th>
						  	<th>title</th>
						  	<th>Slug</th>
							<th>Action</th>
					   	  </tr>
						</thead>   
					<tbody>";
				   foreach ($categories as $kk => $key) {
				    echo "<tr>";
				    echo "<td class='center'>".$kk."</td>";
				    echo "<td class='center'><a href='#'>".$key['name']."</a></td>";
				    echo "<td class='center'><a href='#'>".$key['slug']."</a></td>";
				    echo "<td class='center'>";
				    	echo "<a class='btn btn-success' href=".site_url('admin/edit_category')."/".$key['id'].">";
						echo "<i class='icon-edit icon-white'></i>";
				    	echo "Edit</a>";
				    	echo "<a class='btn btn-danger' href=".site_url('admin/delete_category')."/".$key['id'].">";
				    	echo "<i class='icon-trash icon-white'></i>";
				    	echo "Delete</a>";
				    echo "</td>";
				    echo "</tr>";
						  }
						 

			echo " 
						  </tbody>
					  </table>
				</div>
			</div>";
				} ?>

			</div><!--/#content.span10-->
				</div><!--/fluid-row-->

		<hr>							