<!DOCTYPE html>
<html lang="en">
<head>
	<!--
		Charisma v1.0.0

		Copyright 2012 Muhammad Usman
		Licensed under the Apache License v2.0
		http://www.apache.org/licenses/LICENSE-2.0

		http://usman.it
		http://twitter.com/halalit_usman
	-->
	<meta charset="utf-8">
	<title>Admin Template</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<meta name="author" content="Muhammad Usman">

	<!-- The styles -->
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/bootstrap-cerulean.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/bootstrap-responsive.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/charisma-app.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/fullcalendar.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/fullcalendar.print.css" media='print'>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/chosen.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/uniform.default.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/colorbox.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery.cleditor.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery.noty.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/noty_theme_default.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/elfinder.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/elfinder.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery.iphone.toggle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/opa-icons.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/uploadify.css">
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="img/favicon.ico">
		
</head>

<body>
		<div class="container-fluid">
		<div class="row-fluid">
		
			<div class="row-fluid">
				<div class="span12 center login-header">
					<h2>Login</h2>
				</div><!--/span-->
			</div><!--/row-->
			
		<?php $this->load->view('form_login'); ?>	
			</div><!--/fluid-row-->
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
<!-- jQuery -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- transition / effect library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-tour.js"></script>
	<!-- library for cookie management -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/fullcalendar.min.js"></script>
	<!-- data table plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.dataTables.min.js"></script>

	<!-- chart libraries start -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/excanvas.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.pie.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.stack.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.resize.min.js"></script>

	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.chosen.min.js"></script>

	<!-- checkbox, radio, and file input styler -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.uniform.min.js"></script>

	<!-- plugin for gallery image view -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.colorbox.min.js"></script>

	<!-- rich text editor library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.cleditor.min.js"></script>

	<!-- notification plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.noty.js"></script>

	<!-- file manager library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.elfinder.min.js"></script>

	<!-- star rating plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.raty.min.js"></script>

	<!-- for iOS style toggle switch -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.iphone.toggle.js"></script>

	<!-- autogrowing textarea plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.autogrow-textarea.js"></script>

	<!-- multiple file upload plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.uploadify-3.1.min.js"></script>

	<!-- history.js for cross-browser state change on ajax -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.history.js"></script>

	<!-- application script for Charisma demo -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/charisma.js"></script>
		
		
</body>
</html>