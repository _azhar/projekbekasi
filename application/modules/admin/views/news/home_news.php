
			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
						<h2><i class="icon-list-alt"></i><a href="<?php $this->config->item('base_url')?>admin/add_thread">Create New Thread</a></h2>
					</div>
				
			<?php if (count($get_all_posts)) {
				echo "
				<div class='box-content'>
					<table class='table table-striped table-bordered bootstrap-datatable datatable'>
						<thead>
					 	  <tr>
					 	  	<th>No</th>
						  	<th>title</th>
							<th>Actions</th>
					   	  </tr>
						</thead>   
					<tbody>";
				   foreach ($get_all_posts as $key => $thread) {
				    echo "<tr>";
				    echo "<td class='center'>".$key."</td>";
				    echo "<td class='linkviewtip'><a href=".site_url('forum/talk')."/".$thread->slug.">".$thread->title."</a></td>";
				    echo "<td class='center'>";
				    	echo "<a class='btn btn-success' href=".site_url('admin/news_edit')."/".$thread->id.">";
						echo "<i class='icon-edit icon-white'></i>";
				    	echo "Edit</a>";

				    	echo "<a title='delete' class='btn btn-danger' id='thread_id".$thread->id."'  href=".site_url('admin/news_delete')."/".$thread->id.">";
				    	echo "<i class='icon-trash icon-white'></i>";
				    	echo "Delete</a>";
				    
				    echo "</td>";
				    echo "</tr>";
						  }
						 

			echo " 
						  </tbody>
					  </table>
				</div>
			</div>";
				} ?>

			</div><!--/#content.span10-->
				</div><!--/fluid-row-->

		<hr>							
