<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller
{
	public $page_config  = array();

	function __construct()
	{
		parent::__construct();

		$this->load->model('mposts');
		$this->load->model('mforum');
		$this->load->model('mthread');
		$this->load->model('madmin');
		$this->lang->load('tank_auth');
		$this->load->library('tank_auth_groups','','tank_auth');
	}

	public function index($start = 0)
	{
		if (!$this->tank_auth->is_logged_in()) {

			$this->page_config['base_url']    = site_url('welcome/index/');
       		$this->page_config['uri_segment'] = 3;
        	$this->page_config['total_rows']  = $this->db->count_all(TBL_THREAD);;
        	$this->page_config['per_page']    = 10;
        
        	$this->set_pagination();
        
        	$this->pagination->initialize($this->page_config);
        	$thread['page']    = $this->pagination->create_links();
        	$menu['menunav']=$this->mposts->menunav();
        	$side['categories']    = $this->madmin->category_get_all();
        	$thread['threads'] = $this->mthread->get_all($start, $this->page_config['per_page']);
        	$data['menunav_view']=$this->load->view('main/menunav',$menu,TRUE);
        	$inisial['nama']	= $this->tank_auth->get_username();
			$data['header'] =$this->load->view('main/header',$inisial, TRUE);
			$data['footer'] =$this->load->view('main/footer','', TRUE);
			$data['view'] =$this->load->view('forum/view_forum',$thread, TRUE);
			$data['side'] =$this->load->view('side_menu',$side, TRUE);
			$data['nama']	= 'GUEST';
			$this->load->view('forum/view',$data);


		} else {

			if ($this->tank_auth->is_admin()==TRUE) {

			$this->page_config['base_url']    = site_url('welcome/index/');
       		$this->page_config['uri_segment'] = 3;
        	$this->page_config['total_rows']  = $this->db->count_all(TBL_THREAD);;
        	$this->page_config['per_page']    = 10;
        
        	$this->set_pagination();
        
        	$this->pagination->initialize($this->page_config);
        	$thread['page']    = $this->pagination->create_links();
        	$menu['menunav']=$this->mposts->menunav();
        	$side['categories']    = $this->madmin->category_get_all();
        	$thread['threads'] = $this->mthread->get_all($start, $this->page_config['per_page']);
        	$data['menunav_view']=$this->load->view('main/menunav',$menu,TRUE);
        	$inisial['nama']	= $this->tank_auth->get_username();
			$data['header'] =$this->load->view('main/header',$inisial, TRUE);
			$data['footer'] =$this->load->view('main/footer','', TRUE);
			$data['view'] =$this->load->view('forum/view_forum',$thread, TRUE);
			$data['side'] =$this->load->view('side_menu',$side, TRUE);
			$data['user_id']	= $this->tank_auth->get_user_id();
			$this->load->view('forum/view',$data);

			} else {
				
			$this->page_config['base_url']    = site_url('forum/index/');
       		$this->page_config['uri_segment'] = 3;
        	$this->page_config['total_rows']  = $this->db->count_all(TBL_THREAD);;
        	$this->page_config['per_page']    = 10;
        
        	$this->set_pagination();
        
        	$this->pagination->initialize($this->page_config);
        	$thread['page']    = $this->pagination->create_links();
        	$menu['menunav']=$this->mposts->menunav();
        	$side['categories']    = $this->madmin->category_get_all();
        	$thread['threads'] = $this->mthread->get_all($start, $this->page_config['per_page']);
        	$data['menunav_view']=$this->load->view('main/menunav',$menu,TRUE);
        	$inisial['nama']	= $this->tank_auth->get_username();
			$data['header'] =$this->load->view('main/header',$inisial, TRUE);
			$data['footer'] =$this->load->view('main/footer','', TRUE);
			$data['view'] =$this->load->view('forum/view_forum',$thread, TRUE);
			$data['side'] =$this->load->view('side_menu',$side, TRUE);
			$data['user_id']	= $this->tank_auth->get_user_id();
			$this->load->view('forum/view',$data);

			}
			
		}
	}

	public function set_pagination()
    {
        $this->page_config['first_link']         = '&lsaquo; First';
        $this->page_config['first_tag_open']     = '<li>';
        $this->page_config['first_tag_close']    = '</li>';
        $this->page_config['last_link']          = 'Last &raquo;';
        $this->page_config['last_tag_open']      = '<li>';
        $this->page_config['last_tag_close']     = '</li>';
        $this->page_config['next_link']          = 'Next &rsaquo;';
        $this->page_config['next_tag_open']      = '<li>';
        $this->page_config['next_tag_close']     = '</li>';
        $this->page_config['prev_link']          = '&lsaquo; Prev';
        $this->page_config['prev_tag_open']      = '<li>';
        $this->page_config['prev_tag_close']     = '</li>';
        $this->page_config['cur_tag_open']       = '<li class="active"><a href="javascript://">';
        $this->page_config['cur_tag_close']      = '</a></li>';
        $this->page_config['num_tag_open']       = '<li>';
        $this->page_config['num_tag_close']      = '</li>';
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */