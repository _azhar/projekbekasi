<?php if (!defined('BASEPATH')) exit ('No direct script access allowed');

/**
* 
*/
class Main extends CI_Controller
{
		public $page_config  = array();

	function __construct()
	{
		parent::__construct();	
		$this->load->model('mposts');
		$this->load->library('tank_auth_groups','','tank_auth');
		$this->lang->load('tank_auth');
	}

	function index()
	{
		if ($this->tank_auth->is_logged_in()) {
		$inisial['nama']	= $this->tank_auth->get_username();

		} else {
		$inisial['nama']	= "GUEST";

		}
		
		$basurl = base_url().'main/index';			
		$this->page_config['base_url'] = $basurl;
		$this->page_config['total_rows'] = $this->mposts->posting();
		$this->page_config['per_page'] = 3;
		
		$this->set_pagination();

		$this->pagination->initialize($this->page_config);
		$page['page_link'] = $this->pagination->create_links();

		$this->db->order_by('pubdate','desc');
		$res = $this->db->get('post', $this->page_config['per_page'], $this->uri->segment(3));
		
		$page['page'] = $res->result_array();
		$menu['menunav']=$this->mposts->menunav();
		$data['menunav_view']=$this->load->view('menunav',$menu,TRUE);
		$data['header'] =$this->load->view('header',$inisial, TRUE);

		$widget['teraktual']=$this->mposts->teraktual();
		$widget['inspiratif']=$this->mposts->inspiratif();
		$widget['bermanfaat']=$this->mposts->bermanfaat();
		$widget['menarik']=$this->mposts->menarik();
		$data['widget1']=$this->load->view('widget1', $widget, TRUE);

		$category_news['category'] = $this->mposts->get_all_categories();
		$data['widget2']=$this->load->view('widget2', $category_news,TRUE);
	
		$data['footer'] =$this->load->view('footer','', TRUE);
		$data['slide_of_news']=$this->load->view('slide_of_news','',TRUE);
		$data['live_post']=$this->load->view('live_post',$page,TRUE);

		$this->load->view('template',$data);
	}

	function set_pagination()
	{
		$this->page_config['first_link']         = '&lsaquo; First';
        $this->page_config['first_tag_open']     = '<li>';
        $this->page_config['first_tag_close']    = '</li>';
        $this->page_config['last_link']          = 'Last &raquo;';
        $this->page_config['last_tag_open']      = '<li>';
        $this->page_config['last_tag_close']     = '</li>';
        $this->page_config['next_link']          = 'Next &rsaquo;';
        $this->page_config['next_tag_open']      = '<li>';
        $this->page_config['next_tag_close']     = '</li>';
        $this->page_config['prev_link']          = '&lsaquo; Prev';
        $this->page_config['prev_tag_open']      = '<li>';
        $this->page_config['prev_tag_close']     = '</li>';
        $this->page_config['cur_tag_open']       = '<li class="active"><a href="javascript://">';
        $this->page_config['cur_tag_close']      = '</a></li>';
        $this->page_config['num_tag_open']       = '<li>';
        $this->page_config['num_tag_close']      = '</li>';
	}

	function post($id)
	{
		if ($this->tank_auth->is_logged_in()) {
		$inisial['nama']	= $this->tank_auth->get_username();

		} else {
		$inisial['nama']	= "GUEST";

		}
		

		$menu['menunav']=$this->mposts->menunav();
		$data['menunav_view']=$this->load->view('menunav',$menu,TRUE);
		$data['header'] =$this->load->view('header',$inisial, TRUE);

		$widget['teraktual']=$this->mposts->teraktual();
		$widget['inspiratif']=$this->mposts->inspiratif();
		$widget['bermanfaat']=$this->mposts->bermanfaat();
		$widget['menarik']=$this->mposts->menarik();
		$data['widget1']=$this->load->view('widget1', $widget, TRUE);

		$category_news['category'] = $this->mposts->get_all_categories();
		$data['widget2']=$this->load->view('widget2', $category_news,TRUE);
	
		$data['footer'] =$this->load->view('footer','', TRUE);
		$detail['post'] = $this->mposts->get_posts($id);
		$data['detail_post']=$this->load->view('detail_post',$detail,TRUE);

		$this->load->view('template_post',$data);
	}

	function category($id)
	{
		if ($this->tank_auth->is_logged_in()) {
		$inisial['nama']	= $this->tank_auth->get_username();

		} else {
		$inisial['nama']	= "GUEST";

		}
		
		$basurl = base_url().'main/category/'.$id;			
		$this->page_config['base_url'] = $basurl;
		$this->page_config['total_rows'] = $this->mposts->get_category_total($id);
		$this->page_config['per_page'] = 3;
		$this->page_config['uri_segment'] = 4;
		
		$this->set_pagination();

		$this->pagination->initialize($this->page_config);
		$page['page_link'] = $this->pagination->create_links();
		$res = $this->mposts->get_all_post_categories($this->page_config['per_page'],$this->uri->segment(4),$id);
		$page['page'] = $res->result_array();
		$menu['menunav']=$this->mposts->menunav();
		$data['menunav_view']=$this->load->view('menunav',$menu,TRUE);
		$data['header'] =$this->load->view('header',$inisial, TRUE);

		$widget['teraktual']=$this->mposts->teraktual();
		$widget['inspiratif']=$this->mposts->inspiratif();
		$widget['bermanfaat']=$this->mposts->bermanfaat();
		$widget['menarik']=$this->mposts->menarik();
		$data['widget1']=$this->load->view('widget1', $widget, TRUE);

		$category_news['category'] = $this->mposts->get_all_categories();
		$data['widget2']=$this->load->view('widget2', $category_news,TRUE);
	
		$data['footer'] =$this->load->view('footer','', TRUE);
		$data['live_post']=$this->load->view('live_post',$page,TRUE);

		$this->load->view('category_post',$data);
	}

}	