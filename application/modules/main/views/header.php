<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Potret Bekasi</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/media-queries.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/flexslider.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/gallery.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/slide.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.3.2.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/slide.js" ></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ddaccordion.js" ></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ddaccordion-init.js" ></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/menu-responsive.js" ></script>

	<script src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>

	<script type="text/javascript" charset="utf-8">
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation: "slide",
			controlsContainer: ".flex-container"
		});
	});
	</script>

</head>
<body>
	
<?php $this->load->view('panel'); ?>
	<div id="pagewrap">
		<header id="header">
			<hgroup class="page-header">
				<img src="<?php echo base_url();?>assets/images/potret-bekasi-home_01.jpg?>" width="990" height="144">
			</hgroup>
			<?php $this->load->view('menunav'); ?>
			<form id="searchform">
				<input type="search" id='s' placeholder="search">
			</form>
		</header>
