<section class="widget">
<div class="arrowlistmenu">
	<h3 class="menuheader expandable">Berita Teraktual</h3>
	<ul class="categoryitems">
		<?php
			foreach ($teraktual->result_array() as $row) {
				echo "<li><a href=".$this->config->item('base_url')."main/post/".$row['id'].">".$row['title']."</a></li>";
			}
		?>
	</ul>

	<h3 class="menuheader expandable">Berita Inspiratif</h3>
	<ul class="categoryitems">
		<?php
			foreach ($inspiratif->result_array() as $row) {
				echo "<li><a href=".$this->config->item('base_url')."main/post/".$row['id'].">".$row['title']."</a></li>";
			}
		?>
	</ul>
	
	<h3 class="menuheader expandable">Bermanfaat</h3>
	<ul class="categoryitems">
		<?php
			foreach ($bermanfaat->result_array() as $row) {
				echo "<li><a href=".$this->config->item('base_url')."main/post/".$row['id'].">".$row['title']."</a></li>";
			}
		?>
	</ul>

		<h3 class="menuheader expandable">Menarik</h3>
	<ul class="categoryitems">
		<?php
			foreach ($menarik->result_array() as $row) {
				echo "<li><a href=".$this->config->item('base_url')."main/post/".$row['id'].">".$row['title']."</a></li>";
			}
		?>
	</ul>
	
</div>
</section>
