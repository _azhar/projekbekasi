<div class="judul">
<h2>Slide of News</h2>

<div class="flex-container">
	<div class="flexslider">
		<ul class="slides">
			<li>
				<a href="#kue1">
					<img src="<?php echo base_url(); ?>assets/images/slide1.jpg" />
				</a>
				<p class="flex-caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent malesuada est a libero venenatis vehicula lacinia sapien laoreet!</p>
			</li>
			<li>
				<a href="#kue2">
					<img src="<?php echo base_url(); ?>assets/images/slide2.jpg" />
				</a>
				<p class="flex-caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent malesuada est a libero venenatis vehicula lacinia sapien laoreet!</p>
			</li>
			<li>
				<a href="#kue3">
					<img src="<?php echo base_url(); ?>assets/images/slide3.jpg" />
				</a>
				<p class="flex-caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent malesuada est a libero venenatis vehicula lacinia sapien laoreet!</p>
			</li>
			<li>
				<a href="#kue4">
					<img src="<?php echo base_url(); ?>assets/images/slide4.jpg" />
				</a>
				<p class="flex-caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent malesuada est a libero venenatis vehicula lacinia sapien laoreet!</p>
			</li>
		</ul>
	</div>
</div>

</div>
