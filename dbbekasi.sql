-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 15, 2013 at 04:15 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbbekasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `menunav`
--

CREATE TABLE IF NOT EXISTS `menunav` (
  `id_main` int(11) NOT NULL,
  `nama_menu` varchar(35) NOT NULL,
  `link` varchar(45) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  `id_menu_static` varchar(35) NOT NULL,
  PRIMARY KEY (`id_main`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menunav`
--

INSERT INTO `menunav` (`id_main`, `nama_menu`, `link`, `aktif`, `id_menu_static`) VALUES
(1, 'Home', 'main/home', 'Y', '0'),
(2, 'News', 'main/news', 'Y', '0'),
(3, 'Forum', 'main/forum', 'Y', '0'),
(4, 'Gallery', 'main/gallery', 'Y', '');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('draft','published') COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `pubdate` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `teraktual` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `inspiratif` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `bermanfaat` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `menarik` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `title`, `tags`, `status`, `body`, `category_id`, `pubdate`, `user_id`, `image`, `teraktual`, `link`, `inspiratif`, `bermanfaat`, `menarik`) VALUES
(30, 'Dasar Hukum', 'dasar hukum', 'published', '<p>Progressively e-enable premium web services and quality strategic theme areas. Proactively communicate market positioning portals without cost effective functionalities. Completely incubate turnkey web-readiness vis-a-vis magnetic web services. Proactively harness resource sucking core competencies after client-centric infrastructures. Globally scale backend value before adaptive innovation.</p>\n<div>&nbsp;</div>', 8, '2012-12-27 07:53:28', 2, '', 'N', '#', 'Y', 'Y', 'Y'),
(31, 'Keputusan Presiden', 'hukum nasional', 'published', '<p>Collaboratively iterate standards compliant materials with diverse ideas. Appropriately pursue best-of-breed mindshare and corporate methods of empowerment. Credibly provide access to scalable relationships vis-a-vis state of the art growth strategies. Compellingly plagiarize intermandated mindshare whereas interactive e-commerce. Compellingly administrate dynamic testing procedures via collaborative technologies.</p>\n<div>&nbsp;</div>', 9, '2012-12-27 08:01:19', 2, './uploads/DSC_0031.jpg', 'Y', '#', 'Y', 'Y', 'Y'),
(33, 'Maling digebukin', 'maling', 'published', '<p>Phosfluorescently disintermediate seamless initiatives before enterprise action items. Enthusiastically myocardinate ubiquitous interfaces without collaborative leadership. Synergistically coordinate client-centered schemas vis-a-vis customized results. Interactively coordinate effective quality vectors before go forward ROI. Phosfluorescently foster enterprise niche markets without emerging imperatives.</p>\n<p>&nbsp;</p>\n<p>Professionally pontificate resource maximizing value after e-business solutions. Credibly re-engineer cross-media deliverables vis-a-vis collaborative platforms. Proactively enhance collaborative bandwidth and tactical bandwidth. Phosfluorescently mesh low-risk high-yield supply chains for extensible potentialities. Rapidiously recaptiualize extensible process improvements through out-of-the-box models.</p>\n<p>&nbsp;</p>\n<p>Synergistically revolutionize inexpensive products and magnetic expertise. Globally grow client-focused manufactured products and 2.0 testing procedures. Assertively generate functionalized results through diverse internal or "organic" sources. Objectively facilitate viral best practices without turnkey networks. Dynamically repurpose top-line ROI without mission-critical leadership.</p>\n<p>&nbsp;</p>\n<p>Objectively transition intuitive niches before intuitive technologies. Completely administrate pandemic metrics through state of the art customer service. Continually exploit tactical manufactured products after wireless potentialities. Energistically synergize enabled niches through standards compliant outsourcing. Efficiently maximize enterprise bandwidth via collaborative markets.</p>\n<p>&nbsp;</p>\n<p>Globally exploit technically sound interfaces without orthogonal partnerships. Distinctively facilitate unique web-readiness without best-of-breed materials. Energistically transform client-based intellectual capital via backend markets. Authoritatively initiate performance based solutions rather than functional resources. Dynamically recaptiualize high-payoff web-readiness and cooperative niches.</p>\n<p>&nbsp;</p>\n<p>Efficiently architect granular process improvements via low-risk high-yield best practices. Energistically reintermediate front-end web services for enterprise leadership skills. Seamlessly recaptiualize maintainable solutions with viral materials. Objectively create robust outsourcing vis-a-vis backward-compatible innovation. Monotonectally parallel task optimal web-readiness vis-a-vis orthogonal vortals.</p>\n<p>&nbsp;</p>\n<p>Quickly fashion world-class potentialities rather than granular testing procedures. Authoritatively fabricate high standards in networks via enterprise-wide paradigms. Globally redefine high standards in systems via client-based collaboration and.</p>', 10, '2013-01-03 22:30:33', 2, './uploads/senja.jpg', 'N', '#', 'N', 'N', 'Y'),
(34, 'Maling kejebur', 'maling', 'published', '<p>Interactively visualize sticky mindshare whereas cross-unit infomediaries. Holisticly exploit scalable ideas via strategic potentialities. Seamlessly simplify open-source best practices with distinctive benefits. Appropriately deliver viral relationships via pandemic benefits. Dynamically benchmark vertical value after corporate interfaces.</p>\n<p>&nbsp;</p>\n<p>Competently supply cross-platform niches whereas standardized catalysts for change. Intrinsicly implement user friendly channels through interoperable functionalities. Globally scale go forward information with front-end services. Completely generate seamless content vis-a-vis revolutionary e-markets. Dynamically actualize fully researched ideas rather than process-centric strategic theme areas.</p>\n<p>&nbsp;</p>\n<p>Dynamically reconceptualize leading-edge solutions through collaborative technology. Dynamically formulate empowered "outside the box" thinking before just in time process improvements. Interactively actualize multifunctional initiatives for wireless customer service. Objectively grow adaptive services for market-driven resources. Credibly negotiate global e-services rather than cooperative supply chains.</p>\n<p>&nbsp;</p>\n<p>Assertively visualize adaptive content for synergistic information. Objectively cultivate stand-alone leadership skills whereas intuitive e-commerce. Distinctively reconceptualize global experiences without standards compliant human capital. Enthusiastically actualize 2.0 e-business and equity invested leadership. Energistically redefine 2.0 meta-services via alternative manufactured products.</p>\n<p>&nbsp;</p>\n<p>Globally whiteboard end-to-end users through real-time convergence. Collaboratively develop focused infomediaries via fully researched deliverables. Efficiently incentivize enterprise-wide data after functionalized infomediaries. Phosfluorescently fabricate fully researched e-markets after 24/365 architectures. Synergistically harness alternative intellectual capital vis-a-vis team driven niches.</p>\n<p>&nbsp;</p>\n<p>Assertively streamline end-to-end technology via cross functional e-business. Enthusiastically mesh professional meta-services without inexpensive infomediaries. Uniquely transition process-centric models vis-a-vis open-source technology. Authoritatively fashion interoperable interfaces with collaborative solutions. Holisticly strategize ethical strategic theme areas with dynamic models.</p>\n<p>&nbsp;</p>\n<p>Efficiently create open-source portals without cross-media infrastructures. Enthusiastically grow effective infrastructures through functionalized information. Rapidiously predominate professional alignments without integrated leadership skills. Seamlessly leverage other''s functional initiatives for.</p>', 10, '2013-01-03 22:30:18', 2, './uploads/feeling.jpg', 'Y', '#', 'N', 'N', 'Y'),
(35, 'Maling ngantuk', 'maling', 'published', '<p>Interactively visualize sticky mindshare whereas cross-unit infomediaries. Holisticly exploit scalable ideas via strategic potentialities. Seamlessly simplify open-source best practices with distinctive benefits. Appropriately deliver viral relationships via pandemic benefits. Dynamically benchmark vertical value after corporate interfaces.</p>\n<p>&nbsp;</p>\n<p>Competently supply cross-platform niches whereas standardized catalysts for change. Intrinsicly implement user friendly channels through interoperable functionalities. Globally scale go forward information with front-end services. Completely generate seamless content vis-a-vis revolutionary e-markets. Dynamically actualize fully researched ideas rather than process-centric strategic theme areas.</p>\n<p>&nbsp;</p>\n<p>Dynamically reconceptualize leading-edge solutions through collaborative technology. Dynamically formulate empowered "outside the box" thinking before just in time process improvements. Interactively actualize multifunctional initiatives for wireless customer service. Objectively grow adaptive services for market-driven resources. Credibly negotiate global e-services rather than cooperative supply chains.</p>\n<p>&nbsp;</p>\n<p>Assertively visualize adaptive content for synergistic information. Objectively cultivate stand-alone leadership skills whereas intuitive e-commerce. Distinctively reconceptualize global experiences without standards compliant human capital. Enthusiastically actualize 2.0 e-business and equity invested leadership. Energistically redefine 2.0 meta-services via alternative manufactured products.</p>\n<p>&nbsp;</p>\n<p>Globally whiteboard end-to-end users through real-time convergence. Collaboratively develop focused infomediaries via fully researched deliverables. Efficiently incentivize enterprise-wide data after functionalized infomediaries. Phosfluorescently fabricate fully researched e-markets after 24/365 architectures. Synergistically harness alternative intellectual capital vis-a-vis team driven niches.</p>\n<p>&nbsp;</p>\n<p>Assertively streamline end-to-end technology via cross functional e-business. Enthusiastically mesh professional meta-services without inexpensive infomediaries. Uniquely transition process-centric models vis-a-vis open-source technology. Authoritatively fashion interoperable interfaces with collaborative solutions. Holisticly strategize ethical strategic theme areas with dynamic models.</p>\n<p>&nbsp;</p>\n<p>Efficiently create open-source portals without cross-media infrastructures. Enthusiastically grow effective infrastructures through functionalized information. Rapidiously predominate professional alignments without integrated leadership skills. Seamlessly leverage other''s functional initiatives for.</p>', 10, '2013-01-03 22:30:01', 2, './uploads/btmn.jpg', 'Y', '#', 'N', 'N', 'Y'),
(36, 'Maling sapi', 'maling', 'published', '<p>Interactively visualize sticky mindshare whereas cross-unit infomediaries. Holisticly exploit scalable ideas via strategic potentialities. Seamlessly simplify open-source best practices with distinctive benefits. Appropriately deliver viral relationships via pandemic benefits. Dynamically benchmark vertical value after corporate interfaces.</p>\n<p>&nbsp;</p>\n<p>Competently supply cross-platform niches whereas standardized catalysts for change. Intrinsicly implement user friendly channels through interoperable functionalities. Globally scale go forward information with front-end services. Completely generate seamless content vis-a-vis revolutionary e-markets. Dynamically actualize fully researched ideas rather than process-centric strategic theme areas.</p>\n<p>&nbsp;</p>\n<p>Dynamically reconceptualize leading-edge solutions through collaborative technology. Dynamically formulate empowered "outside the box" thinking before just in time process improvements. Interactively actualize multifunctional initiatives for wireless customer service. Objectively grow adaptive services for market-driven resources. Credibly negotiate global e-services rather than cooperative supply chains.</p>\n<p>&nbsp;</p>\n<p>Assertively visualize adaptive content for synergistic information. Objectively cultivate stand-alone leadership skills whereas intuitive e-commerce. Distinctively reconceptualize global experiences without standards compliant human capital. Enthusiastically actualize 2.0 e-business and equity invested leadership. Energistically redefine 2.0 meta-services via alternative manufactured products.</p>\n<p>&nbsp;</p>\n<p>Globally whiteboard end-to-end users through real-time convergence. Collaboratively develop focused infomediaries via fully researched deliverables. Efficiently incentivize enterprise-wide data after functionalized infomediaries. Phosfluorescently fabricate fully researched e-markets after 24/365 architectures. Synergistically harness alternative intellectual capital vis-a-vis team driven niches.</p>\n<p>&nbsp;</p>\n<p>Assertively streamline end-to-end technology via cross functional e-business. Enthusiastically mesh professional meta-services without inexpensive infomediaries. Uniquely transition process-centric models vis-a-vis open-source technology. Authoritatively fashion interoperable interfaces with collaborative solutions. Holisticly strategize ethical strategic theme areas with dynamic models.</p>\n<p>&nbsp;</p>\n<p>Efficiently create open-source portals without cross-media infrastructures. Enthusiastically grow effective infrastructures through functionalized information. Rapidiously predominate professional alignments without integrated leadership skills. Seamlessly leverage other''s functional initiatives for.</p>', 10, '2013-01-03 22:31:09', 2, './uploads/logo-uin-baru.jpg', 'N', '#', 'N', 'N', 'Y'),
(37, 'Hukuman Mancung', 'hukum', 'published', '<p  justify;">Objectively deploy top-line partnerships whereas B2B opportunities. Assertively incubate team driven metrics with multimedia based schemas. Professionally maintain high-quality leadership for enabled customer service. Energistically iterate dynamic markets after magnetic information. Phosfluorescently matrix dynamic human capital vis-a-vis prospective manufactured products.</p>\n<div>&nbsp;</div>', 9, '2012-12-27 14:00:12', 2, './uploads/DSC_0034.jpg', 'Y', '#', 'N', 'N', 'Y');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
